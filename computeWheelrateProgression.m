function [avgProgression, progressions] = computeWheelrateProgression(wrData,varargin)
%computeWheelrateProgression A one line description of what this code does
% A more detailed explanaion of this code goes here.
%
%
% SYNTAX:
%   argOut1 = computeWheelrateProgression(argIn1,argIn2)
%   [argOut1,argOut2] = computeWheelrateProgression(argIn1,argIn2)
%   [argOut1,argOut2] = computeWheelrateProgression(argIn1,argIn2,varargin1,...)
%   [argOut1,argOut2] = computeWheelrateProgression(argin1,argIn2,Name,Value)
%
% REQUIRED INPUTS:
%   - argIn1 - Describe the first input argument including size and class
%   - exampleArgIn - A double or an (m x n) array of doubles, that
%   represents something relevant to the problem this function solves.
%
% OPTIONAL INPUTS:
%   - argOption1 - An optional input described in the same manner as the
%   required inputs. Be sure to describe the default behavior.
%
%   Advanced users may want to implement optional inputs as "Name,Value"
%   pairs. This can be done using the "inputParser" class. 
%
% OUTPUTS:
%   - argOut1 - Describe the first output argument including size and class
%   - exampleArgOut - A (m x 1) cell array of strings containing some
%   information relevant to the problem this function solves.
%
% EXAMPLES:
%   If required, provide examples of how to use the function here.
%
% See also newFunction, inputParser, function.

% Copyright 2021 Specialized Bicycle Components, Inc.
% Author: mmorrison, mmorrison@specialized.com 

%% Required Input Validation
if nargin < 1
    error('This function requires at least 2 input arguments!')
end

%% sag and bottom out definitions
sag = .33;
bottomOut = .95;

%% Main Function Logic
logSag = find(wrData.sWheelR > sag * wrData.sWheelR(end));
logBottom = find(wrData.sWheelR > bottomOut * wrData.sWheelR(end));

progressions = zeros(1,width(wrData.FWheelR));
for i = 1 : width(wrData.FWheelR)
    progressions(i) = wrData.FWheelR(logBottom(1)-1,i) / wrData.FWheelR(logSag(1)-1,i);
    
end

avgProgression = mean(progressions);
stdProgression = std(progressions);

if stdProgression > 0.1
    warning('Double check spring data, standard deviation of progression between setups is larger than 0.1');
end

end % EOF [computeWheelrateProgression.m]
