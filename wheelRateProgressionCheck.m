%% Progression Checks Across Bikes
% target sag force, 30% WHEEL TRAVEl, same force across bikes
% compute ratio of 30% sag force to 95% sag force (WRR wheelrate ratio)
% figure out what avg is, what is too low, what is too high, what is dialed

ccc;

%% sag and end
sag = .33;
bottomOut = .95;

%% stumpy
size = 'S3';
id = '175248';
sj = BikeConfigs.getCompleteBike(id,size);

sjKin = solveKinematics(sj);
sjLR = computeLR_MR(sjKin);

sjShockCoil = sj.Shock;

springStiffness = 300;
for i=1:length(sjShockCoil.Spring.ProcessedData)
    sjShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * sjShockCoil.Spring.ProcessedData(i).s(end);
    sjShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(sjShockCoil.Spring.ProcessedData(i).s))';
end

sjWRair = computeWheelRate(sjKin,sj.Shock,true);
sjWRcoil = computeWheelRate(sjKin,sjShockCoil,true);

progression_sj_air = computeWheelrateProgression(sjWRair)
progression_sj_coil = computeWheelrateProgression(sjWRcoil)

    
%% stumpy evo
id = '175267';
sjEvo = BikeConfigs.getCompleteBike(id,size);
sjEvoKin = solveKinematics(sjEvo);

sjEvoShockCoil = sjEvo.Shock;
springStiffness = 300;
for i=1:length(sjEvoShockCoil.Spring.ProcessedData)
    sjEvoShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * sjEvoShockCoil.Spring.ProcessedData(i).s(end);
    sjEvoShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(sjEvoShockCoil.Spring.ProcessedData(i).s))';
end

sjEvoWRair = computeWheelRate(sjEvoKin,sjEvo.Shock,true);
sjEvoWRcoil = computeWheelRate(sjEvoKin,sjEvoShockCoil,true);

progression_sjEvo_air = computeWheelrateProgression(sjEvoWRair)
progression_sjEvo_coil = computeWheelrateProgression(sjEvoWRcoil)

%% enduro
id = '171282';
enduro = BikeConfigs.getCompleteBike(id,size);
enduroKin = solveKinematics(enduro);

enduroShockCoil = enduro.Shock;
enduroShockCoil.Spring.Settings.Units = 'lb/in';
springStiffness = 300;
for i=1:length(enduro.Shock.Spring.ProcessedData)
    enduroShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * enduroShockCoil.Spring.ProcessedData(i).s(end);
    enduroShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(enduroShockCoil.Spring.ProcessedData(i).s))';
end

enduroLR_MR = computeLR_MR(enduroKin,true);
enduroWRair = computeWheelRate(enduroKin,enduro.Shock,true);
enduroWRcoil = computeWheelRate(enduroKin,enduroShockCoil,true);

progression_enduro_air = computeWheelrateProgression(enduroWRair)
progression_enduro_coil = computeWheelrateProgression(enduroWRcoil)

%% levo
id = '188252';
levo = BikeConfigs.getCompleteBike(id,size);
levoKin = solveKinematics(levo);

levoShockCoil = levo.Shock;
levoShockCoil.Spring.Settings.Units = 'lb/in';
springStiffness = 300;
for i=1:length(levo.Shock.Spring.ProcessedData)
    levoShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * levoShockCoil.Spring.ProcessedData(i).s(end);
    levoShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(levoShockCoil.Spring.ProcessedData(i).s))';
end

levoLR_MR = computeLR_MR(levoKin,true);
levoWRair = computeWheelRate(levoKin,levo.Shock,true);
levoWRcoil = computeWheelRate(levoKin,levoShockCoil,true);

progression_levo_air = computeWheelrateProgression(levoWRair)
progression_levo_coil = computeWheelrateProgression(levoWRcoil)

%% kenevo
id = '199107';
kenevo = BikeConfigs.getCompleteBike(id,size);
kenevoKin = solveKinematics(kenevo);
kenevoKin.xRA = kenevoKin.RA
kenevoShockCoil = kenevo.Shock;
kenevoShockCoil.Spring.Settings.Units = 'lb/in';
springStiffness = 300;
for i=1:length(kenevoShockCoil.Spring.ProcessedData)
    kenevoShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * kenevoShockCoil.Spring.ProcessedData(i).s(end);
    kenevoShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(kenevoShockCoil.Spring.ProcessedData(i).s))';
end

kenevoLR_MR = computeLR_MR(kenevoKin,true);
kenevoWRair = computeWheelRate(kenevoKin,kenevo.Shock,true);
kenevoWRcoil = computeWheelRate(kenevoKin,kenevoShockCoil,true);

progression_kenevo_air = computeWheelrateProgression(kenevoWRair)
progression_kenevo_coil = computeWheelrateProgression(kenevoWRcoil)

%% epic evo
id = '175238';
size = 'M';
epicEvo = BikeConfigs.getCompleteBike(id,size);
epicEvoKin = solveKinematics(epicEvo);

epicEvoShockCoil = epicEvo.Shock;
epicEvoShockCoil.Spring.Settings.Units = 'lb/in';
springStiffness = 300;
for i=1:length(epicEvoShockCoil.Spring.ProcessedData)
    epicEvoShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * epicEvoShockCoil.Spring.ProcessedData(i).s(end);
    epicEvoShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(epicEvoShockCoil.Spring.ProcessedData(i).s))';
end

epicEvoLR_MR = computeLR_MR(epicEvoKin,true);
epicEvoWRair = computeWheelRate(epicEvoKin,epicEvo.Shock,true);
epicEvoWRcoil = computeWheelRate(epicEvoKin,epicEvoShockCoil,true);

progression_epicEvo_air = computeWheelrateProgression(epicEvoWRair)
progression_epicEvo_coil = computeWheelrateProgression(epicEvoWRcoil)

%% epic
id = '175233';
size = 'M';
epic = BikeConfigs.getCompleteBike(id,size);
epicKin = solveKinematics(epic);

epicShockCoil = kenevo.Shock;
epicShockCoil.Spring.Settings.Units = 'lb/in';
springStiffness = 300;
for i=1:length(epicShockCoil.Spring.ProcessedData)
    epicShockCoil.Spring.ProcessedData(i).Setup = springStiffness;
    springStiffness = springStiffness + 100;
    lbToN = 4.4482216282509;
    inTom = 1/0.0254;
    endForce = springStiffness * lbToN * inTom * epicShockCoil.Spring.ProcessedData(i).s(end);
    epicShockCoil.Spring.ProcessedData(i).F = linspace(0,endForce,length(epicShockCoil.Spring.ProcessedData(i).s))';
end

epicLR_MR = computeLR_MR(epicKin,true);
epicWRair = computeWheelRate(epicKin,epic.Shock,true);
epicWRcoil = computeWheelRate(epicKin,epicShockCoil,true);

[progression_epic_air, tot1] = computeWheelrateProgression(epicWRair)
[progression_epic_coil, tot2] = computeWheelrateProgression(epicWRcoil)

%% clear unnecessary vars
clearvars size ans id

%% check values

















